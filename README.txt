---TO RUN IN DOCKER---

TO BUILD THE DOCKER IMAGE 
docker build . -t dario-araya/api-reign

TO RUN THE IMAGE
docker run -p 8080:3000 dario-araya/api-reign

CREDENTIALS TO GET THE TOKEN (for authorization)
{
    "email": "UserTest@correo.cl",
    "password": "Y29udHJhc2XDsWE="
}

---TO RUN IN LOCAL MACHINE---

TO INSTALL ALL DEPENDENCIES 
npm install

TO RUN PROJECT
npm run start:dev

------IMPORTANT------
The code to connect to the external api once an hour is on posts.service.ts on line 46
---------------------
If there is any doubt with the code or a problem executing it, please contact me at dararayag@gmail.com